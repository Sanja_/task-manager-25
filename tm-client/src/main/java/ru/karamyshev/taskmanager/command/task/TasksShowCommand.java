package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.endpoint.TaskDTO;

import java.util.List;

public class TasksShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tsklst";
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public @NotNull String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllTaskByUserId(session);
        int index = 1;
        for (TaskDTO task : tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
