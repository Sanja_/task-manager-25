package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;


import java.util.List;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvnm";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskOneByName(session, name);
 /*       if (task == null || task.isEmpty()) System.out.println("[FAIL]");*/
         System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles(){
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
