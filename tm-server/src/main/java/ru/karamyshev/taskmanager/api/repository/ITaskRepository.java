package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull Task task);

    @Nullable
    List<Task> findAllByUserId(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByName(@NotNull String userId, @NotNull String name);

    void removeAll();
}
