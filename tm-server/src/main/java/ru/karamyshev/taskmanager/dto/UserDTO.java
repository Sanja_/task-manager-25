package ru.karamyshev.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @Nullable
    private String email = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String lastName = "";

    @Nullable
    private String middleName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLE: " + role;
    }

    @Nullable
    public static UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        @NotNull final List<UserDTO> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            if (user == null) continue;
            result.add(new UserDTO(user));
        }
        return result;
    }

    public UserDTO(@Nullable final User user) {
        if (user == null) return;
        id = user.getId();
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        if (user.getEmail() != null) email = user.getEmail();
        if (user.getFirstName() != null) firstName = user.getFirstName();
        if (user.getMiddleName() != null) middleName = user.getMiddleName();
        if (user.getLastName() != null) lastName = user.getLastName();
        if (user.getRole() != null) role = user.getRole();
        if (user.getLocked() != null) locked = user.getLocked();
    }

}
