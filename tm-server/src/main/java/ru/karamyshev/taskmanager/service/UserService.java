package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.NotLockYourUserException;
import ru.karamyshev.taskmanager.exception.NotMatchPasswordsException;
import ru.karamyshev.taskmanager.exception.NotRemoveYourUserException;
import ru.karamyshev.taskmanager.exception.empty.*;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.repository.UserRepository;
import ru.karamyshev.taskmanager.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new Exception();
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginFailedException();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new Exception();
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final List<User> userList = userRepository.findAll();
        entityManager.close();
        return UserDTO.toDTO(userList);
    }

    @Nullable
    @Override
    public void removeUser(@Nullable final User user) throws Exception {
        if (user == null) throw new Exception();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        try {
            userRepository.removeUser(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginFailedException();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void removeUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (currentLogin.equals(login)) throw new NotRemoveYourUserException();
        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        try {
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllUsers() {
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.removeAllUsers();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);

        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(Role.USER);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);

        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        try {
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);
        user.setRole(role);

        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void lockUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new Exception();
        if (currentLogin.equals(login)) throw new NotLockYourUserException();
        user.setLocked(true);

        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public void unlockUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        @Nullable final User currentUser = findByLogin(currentLogin);
        if (user == null) throw new Exception();
        if (currentUser == null) throw new Exception();
        if (currentLogin.equals(login)) throw new NotLockYourUserException();
        user.setLocked(false);

        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void renamePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String oldPassword = user.getPasswordHash();
        @Nullable final String newHash = HashUtil.salt(newPassword);
        if (newHash == null) throw new Exception();
        if (oldPassword.equals(newHash)) throw new NotMatchPasswordsException();
        user.setPasswordHash(newHash);

        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void renameFirstName(
            @Nullable final String currentUserId,
            @Nullable final String newFirstName
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(currentUserId);
        if (user == null) throw new AccessDeniedException();
        user.setFirstName(newFirstName);

        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
