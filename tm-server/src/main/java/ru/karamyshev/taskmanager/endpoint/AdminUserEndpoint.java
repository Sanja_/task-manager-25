package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IAdminUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws Exception {
        serviceLocator.getUserService().create(login, password, email);

    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @Nullable final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        serviceLocator.getUserService().lockUserByLogin(currentUser.getLogin(), login);
    }

    @Override
    @WebMethod
    public void unlockUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @Nullable final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        serviceLocator.getUserService().unlockUserByLogin(currentUser.getLogin(), login);
    }

    @Override
    @WebMethod
    public void removeUsByLog(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeByLogin(login);
    }

}
