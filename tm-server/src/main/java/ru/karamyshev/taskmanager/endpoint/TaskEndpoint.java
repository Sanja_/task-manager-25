package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.ITaskEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    private IServiceLocator serviceLocator;

    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String projectName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), taskName, projectName);
    }

    @Override
    @WebMethod
    public void createDescription(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String projectName,
            @WebParam(name = "descriptionTask", partName = "descriptionTask") @Nullable String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, projectName, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findAllTaskByUserId(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clearTaskByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> getTaskList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "tasks", partName = "tasks") @Nullable List<Task> tasks
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        //serviceLocator.getTaskService().load(tasks);
    }

}
